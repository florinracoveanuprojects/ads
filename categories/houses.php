<?php
$categoryItems = dbSelect("categories", ['category' => 'houses']);
?>
<form method="post" name="myFormName">
	<select class="form-control" name="houses_category" onchange="this.form.submit()">
		<option value="" disabled selected>Tip</option>
        <option value="all" >Toate</option><?php
		if(isset($_POST['houses_category'])){
			foreach($categoryItems as $categoryItem){?>
				<option value="<?php echo $categoryItem['value']; ?>" <?php if($_POST['houses_category'] == $categoryItem['value']) echo 'selected'; ?>><?php echo $categoryItem['name']; ?></option><?php
			}
		}else{
			foreach($categoryItems as $categoryItem){?>
				<option value="<?php echo $categoryItem['value']; ?>"><?php echo $categoryItem['name']; ?></option><?php
			} 
		}?>
	</select>
	<input type="text" class="form-control" placeholder="Pret de la(euro)">
	<input type="text" class="form-control" placeholder="Pret pana la(euro)">
	<input type="text" class="form-control" placeholder="Suprafata utila de la">
	<input type="text" class="form-control" placeholder="Suprafata utila pana la">
	<select class="form-control">
		<option value="" disabled selected>An constructie</option>
		<option value="petrol">1970 - 1990</option>
		<option value="diesel">1990 - 2000</option>
		<option value="electric">Dupa 2000</option>
	</select>
	<select class="form-control">
		<option value="" disabled selected>Etaj</option>
		<option value="petrol">Demisol</option>
		<option value="diesel">Parter</option>
		<option value="electric">1</option>
		<option value="electric">2</option>
		<option value="electric">3</option>
		<option value="electric">4</option>
		<option value="electric">5</option>
		<option value="electric">6</option>
		<option value="electric">7</option>
		<option value="electric">8</option>
		<option value="electric">9</option>
		<option value="electric">Mansarda</option>
	</select>
	<br />
</form>