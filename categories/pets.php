<?php
$animalItems = dbSelect("categories", ['category' => 'animals']);
?>
<form method="post" name="myFormName">
	<select class="form-control" name="animals_category" onchange="this.form.submit()">
		<option value="" disabled selected>Alege</option>
        <option value="all" >Toate</option><?php
		if(isset($_POST['animals_category'])){
            foreach ($animalItems as $animalItem) { ?>
                <option value="<?php echo $animalItem['value']; ?>" <?php if ($_POST['animals_category'] == $animalItem['value']) {echo 'selected';} ?>><?php echo $animalItem['name']; ?></option><?php
            }
        }else{
            foreach ($animalItems as $animalItem) { ?>
                <option value="<?php echo $animalItem['value']; ?>"><?php echo $animalItem['name']; ?></option><?php
            }
        }?>
	</select>
</form>
<?php

?>