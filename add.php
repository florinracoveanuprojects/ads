<?php
$menuITems = dbSelect('menu');
?>
<form method="post" enctype='multipart/form-data'>
<div class="row justify-content-md-center">
	<div class="col-sm-4">
		<h4>Date despre anunt</h4>
		<h6>Titlu</h6>
		<input type="text" name="title" class="form-control"><br />
            <select class="form-control" name="category" onchange="this.form.submit()">
			    <option disabled selected>Alege categoria</option><?php
                foreach($menuITems as $menuITem){
                    if(isset($_POST['category'])) {?>
                        <option value="<?php echo $menuITem['category']; ?>" <?php if ($_POST['category'] == $menuITem['category']) {echo 'selected';} ?>><?php echo $menuITem['title']; ?></option><?php
                    }else{?>
                        <option value="<?php echo $menuITem['category']; ?>"><?php echo $menuITem['title']; ?></option><?php
                    }
			    }?>
		    </select><br />
        <?php
        if(isset($_POST['category'])){
            if($_POST['category'] == 'auto'){?>
                    <h6>Anul fabricatiei</h6>
                    <input type="text" name="year" class="form-control">
                    <h6>Marca</h6>
                    <input type="text" name="brand" class="form-control">
                    <h6>Model</h6>
                    <input type="text" name="model" class="form-control">
                    <h6>Rulaj</h6>
                    <input type="text" name="km" class="form-control">
                    <h6>Combustibil</h6>
                    <input type="text" name="fuel" class="form-control">
                    <h6>Capacitate cilindrica</h6>
                    <input type="text" name="engine" class="form-control">
                    <h6>Transmisia</h6>
                    <input type="text" name="gearbox" class="form-control">
                </form><?php
            }
        }
        ?>
            <h6>Descriere</h6>
		    <textarea class="form-control"  name="description" aria-label="With textarea"></textarea><br />
		    <h6>Pret</h6>
		    <input type="text" name="price" class="form-control">
		    <input type="checkbox" name="nego" value="Bike">Negociabil<br /><br />
            <br /><h4>Date personale</h4>
		    <h6>Judet</h6>
		    <input type="text" name="county" class="form-control"><br />
            <input type='file' name='file' />

            <button class="btn btn-danger" type="submit" name="announcementAdd">Adauga anuntul</button>
        </form>
	</div>
</div><br />
<?php
if(isset($_POST['announcementAdd'])){
    $name = $_FILES['file']['name'];
    $target_dir = "images/";
    $target_file = $target_dir . basename($_FILES["file"]["name"]);
    $imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
    $extensions_arr = array("jpg","jpeg","png","gif");
    if( in_array($imageFileType,$extensions_arr) ){
        //$query = mysqli_query($mysqlConnect, "INSERT into advertisements(image) values('".$name."')");
        dbInsert("advertisements",['image' => $name ]);
        move_uploaded_file($_FILES['file']['tmp_name'],$target_dir.$name);
    }


}
?>
			