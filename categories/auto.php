<?php
$categoryItems = dbSelect("categories", ['category' => 'auto']);
?>
<form method="post" name="myFormName">
	<select class="form-control" name="auto_category" onchange="this.form.submit()">
		<option value="" disabled selected>Tip</option>
        <option value="all" >Toate</option><?php
			if(isset($_POST['auto_category'])){
				foreach($categoryItems as $categoryItem){?>
					<option value="<?php echo $categoryItem['value']; ?>" <?php if($_POST['auto_category'] == $categoryItem['value']) echo 'selected'; ?>><?php echo $categoryItem['name']; ?></option><?php
				}
			}else{
				foreach($categoryItems as $categoryItem){?>
					<option value="<?php echo $categoryItem['value']; ?>"><?php echo $categoryItem['name']; ?></option><?php
				} 
			}?>
	</select>
	<select class="form-control" name="auto_brand" onchange="this.form.submit()">
		<option value="" disabled selected>Marca</option>
        <option value="all">Toate</option><?php
			if(isset($_POST['auto_category'])){
				$brandItems = dbSelect("auto_brand", ['auto_category' => $_POST['auto_category']]);
				foreach($brandItems as $brandItem){?>
					<option value="<?php echo $brandItem['value']; ?>" <?php if(isset($_POST['auto_brand'])){if($_POST['auto_brand'] == $brandItem['value']) echo 'selected';} ?>><?php echo $brandItem['name']; ?></option><?php
				}
			}else{
				?><option value="">-</option><?php
			
			}?>
	</select>
	<select class="form-control" name="auto_model" onchange="this.form.submit()">
		<option value="" disabled selected>Model</option>
        <option value="all">Toate</option><?php
			if(isset($_POST['auto_brand'])){
				$modelItems = dbSelect("auto_model",['auto_brand' => $_POST['auto_brand']]);
				foreach($modelItems as $modelItem){?>
					<option value="<?php echo $modelItem['value']; ?>" <?php if(isset($_POST['auto_model'])){if($_POST['auto_model'] == $modelItem['value']) echo 'selected';} ?>><?php echo $modelItem['name']; ?></option><?php
				}
			}else{
				?><option value="">-</option><?php
			
			}?>
	</select>
	<input type="text" class="form-control" placeholder="Pret de la(euro)">
	<input type="text" class="form-control" placeholder="Pret pana la(euro)">
	<input type="text" class="form-control" placeholder="An de fabricatie de la">
	<input type="text" class="form-control" placeholder="An de fabricatie pana la">
	<input type="text" class="form-control" placeholder="Rulaj de la">
	<input type="text" class="form-control" placeholder="Rulaj pana la">
	<select class="form-control">
		<option value="" disabled selected>Combustibil</option>
		<option value="petrol">Benzina</option>
		<option value="diesel">Motorina</option>
		<option value="electric">Electric</option>
		<option value="hybrid">Hibrid</option>
	</select>
	<input type="text" class="form-control" placeholder="Capacitate motor de la">
	<input type="text" class="form-control" placeholder="Capacitate motor pana la">
	<select class="form-control" placeholder="Cutie de viteze">
		<option value="" disabled selected>Cutie de viteze</option>
		<option value="manual">Manuala</option>
		<option value="automatic">Automata</option>
	</select><br />
</form>